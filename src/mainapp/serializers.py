from rest_framework import serializers
from .models import *

class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = ('id', 'auditories', 'edge', 'corpus', 'area_type', 'position_top', 'position_left', 'floor')


class EdgeSerializer(serializers.ModelSerializer):
    area_left = serializers.SlugRelatedField( 
        read_only=True,
        slug_field='auditories', 
        source='area.auditories',)
    area_right = serializers.SlugRelatedField( 
        read_only=True,
        slug_field='auditories', 
        source='area.auditories',)
    class Meta:
        model = Edge
        fields = ('id', 'area_left', 'image_left', 'text_left', 'area_right', 'image_right', 'text_right', 'distance')


class CorpusEdgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CorpusEdge
        fields = ('id', 'corpusAbbr_left', 'corpusAbbr_right', 'distance')
# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-25 11:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0006_auto_20180505_1125'),
    ]

    operations = [
        migrations.AddField(
            model_name='edge',
            name='connectingEdge',
            field=models.BooleanField(default=False, verbose_name='\u0421\u043e\u0435\u0434\u0438\u043d\u044f\u0435\u0442 \u043a\u043e\u0440\u043f\u0443\u0441\u0430'),
        ),
    ]

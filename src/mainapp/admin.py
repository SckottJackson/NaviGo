# coding: utf-8
from django.contrib import admin
from .models import *

class AreaAdmin(admin.ModelAdmin):
    list_display = ('auditories', 'corpus', 'area_type')
    search_fields = ('auditories',)

admin.site.register(Area, AreaAdmin)


class EdgeAdmin(admin.ModelAdmin):
    list_display = ('area_left', 'area_right', 'image_left', 'image_right', 'distance', 'corpus')
    search_fields = ('distance',)

admin.site.register(Edge, EdgeAdmin)


# class CorpusEdgeAdmin(admin.ModelAdmin):
#     list_display = ('corpusAbbr_left', 'corpusAbbr_right', 'distance')
#     search_fields = ('corpusAbbr',)

# admin.site.register(CorpusEdge, CorpusEdgeAdmin)


class CorpusAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'floor')
    search_fields = ('name',)

admin.site.register(Corpus, CorpusAdmin)

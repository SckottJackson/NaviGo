# coding: utf-8
# from django.db.models.signals import post_save
from django.contrib.auth.models import User
# from django.dispatch import receiver
# from django.utils import timezone
from django.db import models
from django.conf import settings
from helper import corpus_choises
# from django.db.models.signals import post_delete
# from functools import partial
from django.db.models import Q
# import os



class Corpus(models.Model):
    name = models.CharField(u'Название', max_length=255)
    address = models.CharField(u'Адрес', max_length=255)
    floor = models.IntegerField(u'Этаж', default=1)
    image = models.ImageField(upload_to='uploads/', blank=True, verbose_name='план корпуса')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'корпус'
        verbose_name_plural = u'корпусы'


class Area(models.Model):
    auditories = models.CharField(u'Аудитории', max_length=255, null=True, blank=True)
    edge = models.ManyToManyField('Edge', related_name=u'edge', verbose_name=u'Смежные ребра', null=True, blank=True)
    # corpus = models.CharField(u'Корпус', choices=corpus_choises, max_length=100)
    corpus = models.ForeignKey(Corpus, related_name=u'corpus', verbose_name=u'Корпус')
    area_type = models.CharField(u'Тип области',
        choices=((u"point","Ключевая точка"),(u"connect","Соединительный элемент"),(u"corner","Коридор с поворотом"),(u"straight","Прямой коридор")), max_length=255)
    position_top = models.FloatField(u"Позиция точки сверху", default=1)
    position_left = models.FloatField(u"Позиция точки слева", default=1)
    floor = models.IntegerField(u"Этаж", default=1)

    def get_connectedEdges(self):
        edges = Edge.objects.filter(connectingEdge=True).filter( Q(area_left=self) | Q(area_right=self))

        return edges


    connectedEdges = property(get_connectedEdges)
        
    def __unicode__(self):
        return self.auditories

    class Meta:
        verbose_name = u'область'
        verbose_name_plural = u'области'
    
class Edge(models.Model):
    area_left = models.ForeignKey(Area, related_name=u'area_left', verbose_name=u'Область А')
    image_left = models.ImageField(verbose_name=u'Картинка, ведущая в область А', null=True, blank=True)
    text_left = models.TextField(u'Текст А', null=True, blank=True)
    area_right = models.ForeignKey(Area, related_name=u'area_right', verbose_name=u'Область В')
    image_right = models.ImageField(verbose_name=u'Картинка, ведущая в область В', null=True, blank=True)
    text_right = models.TextField(u'Текст В', null=True, blank=True)
    distance = models.IntegerField(u'Расстояние', default=0)
    corpus = models.ForeignKey(Corpus, related_name=u'corpus2', verbose_name=u'Корпус')
    connectingEdge = models.BooleanField(u"Соединяет корпуса", default=False)
    blocked = models.BooleanField(u"Заблокировать проход", default=False)

    def get_area_left(self):
        return Area.objects.filter(pk=self.area_left.id).first().area_type

    area_type_left = property(get_area_left)

    def get_area_right(self):
        return Area.objects.filter(pk=self.area_right.id).first().area_type

    area_type_right = property(get_area_right)

    def __unicode__(self):
        return u'%s - %s' % (self.area_left, self.area_right)

    class Meta:
        verbose_name = u'ребро'
        verbose_name_plural = u'ребра'


class CorpusEdge(models.Model):
    corpusAbbr_left = models.CharField(u'Корпус слева', max_length=25)
    corpusAbbr_right = models.CharField(u'Корпус справа', max_length=25)
    distance = models.IntegerField(u'Расстояние', default=0)

    def __unicode__(self):
        return self.corpusAbbr_left + " - " + self.corpusAbbr_right

    class Meta:
        verbose_name = u'корпус'
        verbose_name_plural = u'корпуса'

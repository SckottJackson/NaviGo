# coding: utf-8
from __future__ import unicode_literals
from django.views.decorators.http import require_http_methods
from django.db.models.signals import post_delete, post_save
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import views as auth_views
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.core.files import File
from django.dispatch import receiver
from django.core import serializers
from django.core.serializers import serialize
from rest_framework.response import Response
from rest_framework import viewsets
from django.contrib import messages
from rest_framework.decorators import api_view
from itertools import combinations
from rest_framework import status
from django.conf import settings
from django.db.models import Q
from serializers import *
import unicodedata as UD
import urllib, urllib2
from settings import *
from .models import *
from multiprocessing import Pool
from PIL import Image, ImageDraw
from datetime import datetime
import time
import mysql.connector
import json

from cStringIO import StringIO
import subprocess
import chardet
import shutil
import sys
#from django.core.serializers.json import DjangoJSONEncoder
import re
import os
from django.core.files.base import ContentFile
import argparse
import xml.etree.ElementTree as ET
import networkx as nx



class AreaViewSet(viewsets.ModelViewSet):
    queryset = Area.objects.all()
    serializer_class = AreaSerializer

    def get_queryset(self):
        return Area.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class EdgeViewSet(viewsets.ModelViewSet):
    queryset = Edge.objects.all()
    serializer_class = EdgeSerializer

    def get_queryset(self):
        return Edge.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CorpusEdgeViewSet(viewsets.ModelViewSet):
    queryset = CorpusEdge.objects.all()
    serializer_class = EdgeSerializer

    def get_queryset(self):
        return CorpusEdge.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class StaticGraph():
    G = nx.Graph()
    CG = nx.Graph()


def initGraph():
    allEdges = Edge.objects.filter(blocked=False)
    for edge in allEdges:
        StaticGraph.G.add_edge(int(edge.area_left.id), int(edge.area_right.id), weight=int(edge.distance), corpus=edge.corpus, id = edge.id)
    return []


def initCorpusGraph():
    allEdges = CorpusEdge.objects.all()
    for edge in allEdges:
        StaticGraph.CG.add_edge(edge.corpusAbbr_left, edge.corpusAbbr_right, weight=edge.distance)
    return []



@csrf_exempt
@require_http_methods(["POST",])
def get_areas(request):
    img_url = u"%s" % request.POST.get('img_url')
    print(img_url)
    img_file = urllib.urlopen(img_url)
    im = StringIO(img_file.read())
    image = Image.open(im)
    img_width, img_height = image.size
    floor = request.POST.get('floor')
    corpus = request.POST.get('corpus')
    areas_queryset = Area.objects.filter(floor = floor, corpus = corpus)
    edges_queryset = Edge.objects.filter(connectingEdge=False).filter(Q(area_right__in=areas_queryset) | Q(area_left__in=areas_queryset))
    areas = []
    edges = []

    for area in areas_queryset:
        areas.append({ "group": "nodes", "data": { "id": area.auditories, "item_id": area.id, "area_type": area.area_type }, "position": { "x": area.position_left, "y": area.position_top } })

    for edge in edges_queryset:
        edges.append({ "group": "edges", "data": {
                    "id": u'edge%s' % (edge.id),
                    "item_id": edge.id,
                    "source": Area.objects.get(id = edge.area_left_id).auditories,
                    "target": Area.objects.get(id = edge.area_right_id).auditories,
                    #"image_left": edge.image_left,
                    "text_left": edge.text_left,
                    #"image_right": edge.image_right,
                    "text_right": edge.text_right,
                    "distance": edge.distance,
                    "blocked": edge.blocked
                } })


    return JsonResponse({"areas": areas, "edges": edges, "img_width": img_width, "img_height": img_height }, safe=False)



@csrf_exempt
@require_http_methods(["GET",])
def get_corpuses(request):

    corpuses_queryset = Corpus.objects.all()

    result = []
    for corpus in corpuses_queryset:
        result.append({ 
            "id": corpus.id,
            "name": corpus.name,
            "floor": corpus.floor,
            "address": corpus.address,
            "image": corpus.image.url
        })


    return JsonResponse(result, safe=False)


@csrf_exempt
@require_http_methods(["POST",])
def get_connected_nodes(request):

    connectedEdges = Edge.objects.filter(connectingEdge=True).filter( Q(area_left=request.POST.get('area_id')) | Q(area_right=request.POST.get('area_id')))
    # Area.objects.get(id=request.POST.get('area_id')).get_connectedEdges

    result = []
    for edge in connectedEdges:
        result.append({ 
            "id": u'edge%s' % (edge.id),
            "item_id": edge.id,
            "source": Area.objects.get(id = edge.area_left_id).auditories,
            "target": Area.objects.get(id = edge.area_right_id).auditories,
            #"image_left": edge.image_left,
            "text_left": edge.text_left,
            #"image_right": edge.image_right,
            "text_right": edge.text_right,
            "distance": edge.distance,
            "blocked": edge.blocked
        })


    return JsonResponse(result, safe=False)


@csrf_exempt
@require_http_methods(["POST",])
def add_edit_area(request):
    auditories = request.POST.get('auditories')
    edge = request.POST.get('edge')
    corpus = request.POST.get('corpus')
    area_type = request.POST.get('area_type')
    position_top = request.POST.get('position_top')
    position_left = request.POST.get('position_left')
    floor = request.POST.get('floor')

    isNewNode = request.POST.get('isNewNode')

    if isNewNode == '1':
        area = Area(auditories = auditories,
                    corpus_id = corpus, area_type = area_type, position_top = position_top, 
                    position_left = position_left, floor = floor)

    else:
        area_id = request.POST.get('area_id')
        area = Area.objects.get(id = area_id)
        area.auditories = auditories
        area.edge = edge
        area.area_type = area_type
    
    area.save()

    return JsonResponse({"title": area.auditories, "id": area.id, "area_type": area.area_type, "position_left": area.position_left, "position_top": area.position_top}, safe=False)


@csrf_exempt
@require_http_methods(["POST",])
def change_position_area(request):

    area_id = request.POST.get('area_id')
    area = Area.objects.get(id = area_id)
    area.position_top = request.POST.get('position_top')
    area.position_left = request.POST.get('position_left')
    
    area.save()

    return JsonResponse('success', safe=False)



@csrf_exempt
@require_http_methods(["POST",])
def get_connect_nodes(request):
    corpus = request.POST.get('corpus')
    areas_queryset = Area.objects.filter(corpus_id = corpus, area_type="connect")
    areas = []

    for area in areas_queryset:
        areas.append({ "name": area.auditories, "item_id": area.id})


    return JsonResponse(areas, safe=False)


@csrf_exempt
@require_http_methods(["POST",])
def connect_nodes(request):
    area_left_id = request.POST.get('area_left_id')
    #image_left = request.POST.get('image_left')
    # text_left = request.POST.get('text_left')
    area_right_id = request.POST.get('area_right_id')
    #image_right = request.POST.get('image_right')
    # text_right = request.POST.get('text_right')
    # distance = request.POST.get('distance')
    corpus = request.POST.get('corpus')

    source_area = Area.objects.get(id = area_left_id)
    target_area = Area.objects.get(id = area_right_id)


    edge = Edge(area_left = source_area,
                #image_left = image_left,
                # text_left = "",
                area_right = target_area,
                #image_right = image_right,
                # text_right = "",
                # distance = 0,
                corpus_id = corpus,
                connectingEdge=True)

    edge.save()

    return JsonResponse("success", safe=False)



@csrf_exempt
@require_http_methods(["POST",])
def delete_area(request):
    area = Area.objects.get(id = request.POST.get('node_id'))

    area.delete()

    return JsonResponse("deleted", safe=False)




@csrf_exempt
@require_http_methods(["POST",])
def add_edit_edge(request):
    area_left_id = request.POST.get('area_left_id')
    #image_left = request.POST.get('image_left')
    text_left = request.POST.get('text_left')
    area_right_id = request.POST.get('area_right_id')
    #image_right = request.POST.get('image_right')
    text_right = request.POST.get('text_right')
    distance = request.POST.get('distance')
    corpus = request.POST.get('corpus')

    source_area = Area.objects.get(id = area_left_id)
    target_area = Area.objects.get(id = area_right_id)

    isNewEdge = request.POST.get('isNewEdge')
    print(isNewEdge)

    if isNewEdge == '1':
        edge = Edge(area_left = source_area,
                    #image_left = image_left,
                    text_left = text_left,
                    area_right = target_area,
                    #image_right = image_right,
                    text_right = text_right,
                    distance = distance,
                    corpus_id = corpus)

    else:
        edge_id = request.POST.get('edge_id')
        edge = Edge.objects.get(id = edge_id)
        print(edge)
        print(edge.area_left)
        print(source_area)
        edge.area_left = source_area
        #edge.image_left = image_left
        edge.text_left = text_left
        edge.area_right = target_area
        #edge.image_right = image_right
        edge.text_right = text_right
        edge.distance = distance
        edge.corpus_id = corpus
    
    edge.save()

    return JsonResponse({"source": source_area.auditories,
                            "target": target_area.auditories,
                            "text_left": edge.text_left,
                            "text_right": edge.text_right,
                            "item_id": edge.id,
                            "blocked": edge.blocked,
                            "id": u'edge%s' % (edge.id)
                        }, safe=False)



@csrf_exempt
@require_http_methods(["POST",])
def delete_edge(request):
    edge = Edge.objects.get(id = request.POST.get('edge_id'))

    edge.delete()

    return JsonResponse("deleted", safe=False)


@csrf_exempt
@require_http_methods(["POST",])
def block_edge(request):
    edge = Edge.objects.get(id = request.POST.get('edge_id'))

    edge.blocked = not edge.blocked
    edge.save()

    return JsonResponse("success", safe=False)



@csrf_exempt
@require_http_methods(["POST",])
def add_update_courpus(request):
    if request.POST['mode'] == 'create':
        corpus = Corpus(name = request.POST['name'],
                    address = request.POST['address'],
                    image = request.FILES['image'], 
                    floor = request.POST['floor'])
    else:
        corpus = Corpus.objects.filter(id = request.POST['corpus_id'])
        corpus.name = request.POST['name']
        corpus.address = request.POST['address']
        corpus.image = request.FILES['image']
        corpus.floor = request.POST['floor']
    
    corpus.save()

    return JsonResponse('added', safe=False)


@csrf_exempt
@require_http_methods(["POST",])
def get_auditories_for_form(request):
    list_result=[]
    list_result2=[]
    areas = Area.objects.filter(area_type='point').values_list('auditories')
    for i in areas:
        list_result.append({'name': u'%s' % (i[0])})
        list_result2.append(i[0])
    # f = open('%s/static/js/test.json' % (settings.BASE_DIR), 'w+')
    # f.write(json.dumps(list_result))
    # f.close()
    
    return JsonResponse(list_result2, safe=False)




@csrf_exempt
@require_http_methods(["POST",])
def find_way(request):

    _height = request.POST.get('_height')
    _width = request.POST.get('_width')

    _from = request.POST.get('from')
    _to = request.POST.get('to')

    node_from = Area.objects.filter(auditories__contains=_from).first()
    node_to = Area.objects.filter(auditories__contains=_to).first()

    preprocessed_way=[]
    processed_way=[]
    pre_current_node = None
    
    current_corpus = None
    current_floor = ""
    count_slides = -1
    way = nx.shortest_path(StaticGraph.G, int(node_from.id), int(node_to.id), weight='weight')
    for i in way:
        temp_area = Area.objects.filter(pk=i).first()
        if current_corpus != temp_area.corpus or current_floor != str(temp_area.floor):
            current_floor = str(temp_area.floor)
            count_slides += 1
            # print(temp_area.corpus.image.url)
            # image = Image.open("%s/public%s" % (settings.BASE_DIR, temp_area.corpus.image.url))
            img_width = temp_area.corpus.image.width
            img_height = temp_area.corpus.image.height
            # print('img_width', img_width)
            # print('img_height', img_height)
            if float(_width)/img_width <= float(_height)/img_height:
                zooming = float(_width)/img_width
            else:
                zooming = float(_height)/img_height
            # print(zooming)
            if current_corpus != None and current_corpus.address != temp_area.corpus.address:
                print(temp_area.corpus.address)
                preprocessed_way.append({
                    "yandexAPI": True,
                    "width": '%spx' %  _width,
                    "height": '%spx' %  _height,
                    "points": ['Екатеринбург, %s' % current_corpus.address, 'Екатеринбург, %s' % temp_area.corpus.address]
                    # "to": temp_area.corpus.address
                })
                count_slides +=1
            current_corpus = temp_area.corpus
            preprocessed_way.append({
                "image": "http://192.168.0.61:80%s" % (temp_area.corpus.image.url),
                "nodes": [],
                "img_width": '%spx' % (img_width*zooming),
                "img_height": '%spx' % (img_height*zooming),
                "zooming": zooming
            })
        # if not (pre_current_node and
        #         temp_area.area_type == pre_current_node.area_type and 
        #         temp_area.area_type == "straight"):
        #     if pre_current_node:
        #         print(pre_current_node.area_type, temp_area.area_type)
        #     else:
        #         print(temp_area.area_type)
        preprocessed_way[count_slides]['nodes'].append({
            "data": {'item_id': temp_area.id, 'id': temp_area.auditories, 'area_type': temp_area.area_type },
            "group": "nodes",
            "position": {'y': temp_area.position_top*zooming, 'x': temp_area.position_left*zooming}
        })
        pre_current_node = temp_area
    
    return JsonResponse(preprocessed_way, safe=False)



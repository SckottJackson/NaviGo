
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '2szivy40jg9^_e5ox*822sdzj(zfm2ydod&5g7b^p!g8-um56&'

DEBUG = True

ALLOWED_HOSTS = ['*']

# development_mode = True

# try:
#     development_mode = os.environ['DEVELOPMENT_MODE']
#     print('In develop mode')
# except Exception as error:
#     print(error)
#     print('In production mode')

INSTALLED_APPS = [
    'django.contrib.sites',
    'suit',
    'mainapp.apps.MainappConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'rest_framework',
]


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated',),
    # 'DEFAULT_PAGINATION_CLASS': None
    'PAGE_SIZE': 20000
}

SESSION_COOKIE_AGE = 2592000


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'wsgi.application'

# db_password = '123wsxahz990-'

# if development_mode:
db_password = '1234'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'navigoDB',
        'USER': 'root',
        'PASSWORD': db_password,
        'HOST': 'localhost',
        'PORT': '3306',
        'OPTIONS' : {
            'local_infile':1
        }
    }
}

DOMAIN = 'http://192.168.0.61:80'

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

STATIC_ROOT = os.path.abspath(BASE_DIR + '/public/static/')

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.abspath(BASE_DIR + '/public/media/')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

#STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'
#STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'
# Email
# EMAIL_HOST = 'smtp.yandex.ru'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = 'admin@NaviGo.ru'
# EMAIL_HOST_PASSWORD = 'adminNaviGo'
# EMAIL_USE_TLS = True
# DEFAULT_FROM_EMAIL = 'no_reply <admin@NaviGo.ru>'
#CELERY_BROKER_URL = 'redis://'
#CELERY_ACCEPT_CONTENT = ['json']
#CELERY_RESULT_BACKEND = 'redis://'
#CELERY_TASK_SERIALIZER = 'json'

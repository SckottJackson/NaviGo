
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView
from django.conf.urls.static import static
from django.conf.urls import url, include
from rest_framework import routers
from django.contrib import admin
from django.conf import settings
from mainapp import views


router = routers.DefaultRouter()
router.register(r'area', views.AreaViewSet)
router.register(r'edge', views.EdgeViewSet)
router.register(r'corpus_edge', views.CorpusEdgeViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^map-editor', TemplateView.as_view(template_name='mapEditor.html'), name='mapEdinor'),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^find_way/$', views.find_way, name="find_way"),


    url(r'^add_update_courpus/$', views.add_update_courpus, name="add_update_courpus"),

    url(r'^get_areas/$', views.get_areas, name="get_areas"),
    url(r'^get_connect_nodes/$', views.get_connect_nodes, name="get_connect_nodes"),
    url(r'^connect_nodes/$', views.connect_nodes, name="connect_nodes"),

    url(r'^get_connected_nodes/$', views.get_connected_nodes, name="get_connected_nodes"),
    url(r'^get_corpuses/$', views.get_corpuses, name="get_corpuses"),
    url(r'^get_auditories_for_form/$', views.get_auditories_for_form, name="get_auditories_for_form"),

    url(r'^add_edit_area/$', views.add_edit_area, name="add_edit_area"),
    url(r'^change_position_area/$', views.change_position_area, name="change_position_area"),
    url(r'^add_edit_edge/$', views.add_edit_edge, name="add_edit_edge"),
    url(r'^block_edge/$', views.block_edge, name="block_edge"),

    url(r'^delete_area/$', views.delete_area, name="delete_area"),
    url(r'^delete_edge/$', views.delete_edge, name="delete_edge"),

    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

views.initCorpusGraph()
views.initGraph()

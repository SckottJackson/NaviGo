$(document).ready(function () {
  
	$('label.tree-toggler').click(function () {
		$(this).parent().children('ul.tree').toggle(300);
  });
  
  $("#cy").on('dblclick',function(e) {
    x = e.offsetX==undefined?e.layerX:e.offsetX;
    y = e.offsetY==undefined?e.layerY:e.offsetY;
    navigo.x(x/navigo.zoom());
    navigo.y(y/navigo.zoom());
    navigo.modalTitle('Создать вершину');
    navigo.title('');
    navigo.areaType('');
    $('select').material_select();
    navigo.flagForEditNode(false);
    $('#add_area').modal('open');
  });

  cy.on('click', function(event){
    if(navigo.creationEdgeMode() && event.target.data && event.target.group() == 'nodes'){
      // debugger;
      if(navigo.area_left() && event.target.data().item_id != navigo.area_left().data.item_id){
        navigo.area_right(navigo.nodes().filter(function(a){
          return a.data.item_id == event.target.data().item_id
        })[0]);
        cy.elements('node[item_id='+navigo.area_right().data.item_id+']')[0].addClass('selectedNode');
        navigo.flagForEditEdge(false); 
        navigo.modalTitle('Соединить ' + navigo.area_left().data.id + ' с ' + navigo.area_right().data.id + ' ?'); 
        $('#add_edge2').modal('open')
      } else {
        navigo.area_left(navigo.nodes().filter(function(a){
          return a.data.item_id == event.target.data().item_id
        })[0]);
        cy.elements('node[item_id='+navigo.area_left().data.item_id+']')[0].addClass('selectedNode');
      }
    } else if(navigo.deletionEdgeMode() && event.target.data && event.target.group() == 'edges'){
      navigo.editingEdge({data:event.target.data(), droup:"edges"});
      navigo.deleteEdge();
    } else if(navigo.blockEdgeMode() && event.target.data && event.target.group() == 'edges'){
      navigo.blockEdge(event.target.data());
    }
  })

  // cy.on('cxttap', function(e){
  //   console.log('11111')
  // })

  cy.on('mouseup', function(event){
    if(navigo.isNodeDraging() && event.target.data && event.target.group() == 'nodes'){
      navigo.isNodeDraging(false);
      cy.one('drag', dragHandler);
      // debugger;
      $.ajax({
        type: 'POST',
        url: '/change_position_area/',
        data: {
          "area_id": event.target.data().item_id,
          "position_top": event.position.y,
          "position_left": event.position.x,
          "isNewNode": 0
        },
        success: function() {},
        error: function(){}
    })


    }
  });

  cy.one('drag', dragHandler);

  function dragHandler(){
    navigo.isNodeDraging(true);
    console.log('a');
  }

  cy.on('cxttap', function(event){
    // target holds a reference to the originator
    // of the event (core or element)
    var evtTarget = event.target;
    navigo.x(event.position.x/navigo.zoom());
    navigo.y(event.position.y/navigo.zoom());
    console.log(navigo.x() + "__"+ navigo.y())
    if( evtTarget === cy ){
        console.log('tap on background');
    } else if(evtTarget.group()=='nodes'){
        console.log(evtTarget._private)
        navigo.editingNode(navigo.nodes().filter(function(a){ return a.data.item_id == evtTarget._private.data.item_id})[0])
        navigo.modalTitle('Редактировать вершину')
        navigo.title(evtTarget._private.data.id)
        navigo.areaType(navigo.areaTypes().filter(function(a){return a.key == navigo.editingNode().data.area_type})[0]);
        $('select').material_select();
        navigo.flagForEditNode(true)
        if(evtTarget.data().area_type == 'connect'){
          $.ajax({
            type: 'POST',
            url: '/get_connected_nodes/',
            data: {
              "area_id": evtTarget.data().item_id
            },
            success: function(responce) {
              // console.log(responce);
              navigo.connectedNodes(responce);
              $('#add_area').modal('open');
            },
            error: function(){}
          })
        } else {
          $('#add_area').modal('open')
        }

      //navigo.editNote()
      }
    // } else if(evtTarget.group() == 'edges'){
    //   console.log(evtTarget._private)
    //   navigo.editingEdge(navigo.edges().filter(function(a){ return a.data.item_id == evtTarget._private.data.item_id})[0])
    //   navigo.modalTitle('Редактировать ребро')
    //   navigo.title(evtTarget._private.data.id)
    //   navigo.area_left(navigo.nodes().filter(function(a){ return a.data.id ==navigo.editingEdge().data.source})[0])
    //   //navigo.image_left(navigo.editingEdge().data.image_left)
    //   navigo.text_left(navigo.editingEdge().data.text_left)
    //   navigo.area_right(navigo.nodes().filter(function(a){ return a.data.id ==navigo.editingEdge().data.target})[0])
    //   //navigo.image_right(navigo.editingEdge().data.image_right)
    //   navigo.text_right(navigo.editingEdge().data.text_right)
    //   navigo.distance(navigo.editingEdge().data.distance)
    //   navigo.flagForEditEdge(true)
    //   $('#add_edge').modal('open');
    // }
  });


  cy.on('mouseover', 'node', function(e){
    // debugger;
    if(navigo.creationEdgeMode()  && e.target.data && e.target.group() == 'nodes'){
      cy.elements('node[item_id='+e.target.data().item_id+']')[0].addClass('onHoverTargetEdge');
      $('html,body').css('cursor', 'cell');
    }
  });
  
  cy.on('mouseout', 'node', function(e){
    if(navigo.creationEdgeMode()  && e.target.data && e.target.group() == 'nodes'){
      cy.elements('node[item_id='+e.target.data().item_id+']')[0].removeClass('onHoverTargetEdge');  
      $('html,body').css('cursor', 'default');
    }
  });


  cy.on('mouseover', 'edge', function(e){
    if(navigo.deletionEdgeMode() && e.target.data && e.target.group() == 'edges' && navigo.edges().filter(function(a){ return a.data.id == e.target.data().id}).length == 1 ){
      cy.elements('edge[item_id='+e.target.data().item_id+']')[0].addClass('onHoverTargetNode');
      $('html,body').css('cursor', 'cell');
    } else if(navigo.blockEdgeMode() && e.target.data && e.target.group() == 'edges' && navigo.edges().filter(function(a){ return a.data.id == e.target.data().id}).length == 1 ){
      cy.elements('edge[item_id='+e.target.data().item_id+']')[0].addClass('onHoverTargetNode');
      $('html,body').css('cursor', 'cell');
    }
  });

  cy.on('mouseout', 'edge', function(e){
    if(navigo.deletionEdgeMode()  && e.target.data && e.target.group() == 'edges' && navigo.edges().filter(function(a){ return a.data.id == e.target.data().id}).length == 1 ){
      cy.elements('edge[item_id='+e.target.data().item_id+']')[0].removeClass('onHoverTargetNode');
      $('html,body').css('cursor', 'default');
    } else  if(navigo.blockEdgeMode()  && e.target.data && e.target.group() == 'edges' && navigo.edges().filter(function(a){ return a.data.id == e.target.data().id}).length == 1 ){
      cy.elements('edge[item_id='+e.target.data().item_id+']')[0].removeClass('onHoverTargetNode');
      $('html,body').css('cursor', 'default');
    }
  });
  


  $(".dropdown-button").dropdown({belowOrigin: true});
  $("#nav_menu").sideNav();

  $('select').material_select({belowOrigin: true});
});


navigo.setCreationEdgeMode = function(){
  navigo.creationEdgeMode(true);
  navigo.deletionEdgeMode(false);
  navigo.defaultMode(false);
  navigo.blockEdgeMode(false);
}

navigo.setDeletionEdgeMode = function(){
  navigo.creationEdgeMode(false);
  navigo.deletionEdgeMode(true);
  navigo.defaultMode(false);
  navigo.blockEdgeMode(false);
  navigo.dropSelection();
}

navigo.setDefaultMode = function(){
  navigo.creationEdgeMode(false);
  navigo.deletionEdgeMode(false);
  navigo.defaultMode(true);
  navigo.blockEdgeMode(false);
  navigo.dropSelection();
}

navigo.setBlockEdgeMode = function(){
  navigo.creationEdgeMode(false);
  navigo.deletionEdgeMode(false);
  navigo.defaultMode(false);
  navigo.blockEdgeMode(true);
  navigo.dropSelection();
}


navigo.blockEdge = function(data){
  $.ajax({
    type: 'POST',
    url: '/block_edge/',
    data: {
      "edge_id": data.item_id
    },
    success: function () {
      var temp_edge = navigo.edges().filter(function(a){return a.data.item_id == data.item_id})[0];
      temp_edge.data.blocked = !temp_edge.data.blocked;
      cy.elements('edge[item_id='+temp_edge.data.item_id+']')[0].data(temp_edge);
      if(data.blocked){
        cy.elements('edge[item_id='+temp_edge.data.item_id+']')[0].addClass('blocked');
      } else {
        cy.elements('edge[item_id='+temp_edge.data.item_id+']')[0].removeClass('blocked');
      }
    },
    error: function(xhr, ajaxOptions, thrownError){
        console.log('blockEdge.post');
    }
  })
}

navigo.getPoints = ko.computed(function() {
  if(navigo.floor().val && navigo.corpus().val){
    img_url = '/public/media/' + navigo.corpus().img_path + navigo.floor().img_path;
      $('#cy').css('background-image', "url('http://192.168.0.61:80/media/" + navigo.corpus().img_path + navigo.floor().img_path + "')")
      $.ajax({
          type: 'POST',
          url: '/get_areas/',
          data: {
              "floor":navigo.floor().val,
              "corpus":navigo.corpus().val,
              "img_url":img_url
          },
          success: function (responce) {
              $('#cy').css({'width':responce.img_width,'height': responce.img_height})
              cy.remove(cy.elements())
              console.log(responce.areas)
              navigo.nodes(responce.areas)
              var eles = cy.add(responce.areas);

              console.log(responce.edges)
              navigo.edges(responce.edges)
              var eles = cy.add(responce.edges);
          },
          error: function(xhr, ajaxOptions, thrownError){
              console.log('getPoints.post');
          }
      })
  }
});


navigo.dropSelection = function(){
  if(navigo.area_left()){
    if(navigo.area_left().data.area_type == "point"){
      console.log(cy.elements('node[item_id='+navigo.area_left().data.item_id+']')[0]);
      cy.elements('node[item_id='+navigo.area_left().data.item_id+']')[0].classes('area');
    }
    else
      cy.elements('node[item_id='+navigo.area_left().data.item_id+']')[0].classes('');
  }
  if(navigo.area_right()){
    if(navigo.area_right().data.area_type == "point")
      cy.elements('node[item_id='+navigo.area_right().data.item_id+']')[0].classes('area');
    else
      cy.elements('node[item_id='+navigo.area_right().data.item_id+']')[0].classes('');
  }
  navigo.area_left(null);
  navigo.area_right(null);
  $('html,body').css('cursor', 'default');
}

$('#add_edge2').on('hide.bs.modal', function () {
  navigo.dropSelection();
})
$('#connect_node').on('hide.bs.modal', function () {
  navigo.selectedCorpusModal(null);
  navigo.connectNodesList([]);
})

navigo.hideShowMenu = function(){
  navigo.isMenuHidden(!navigo.isMenuHidden());
}

navigo.deleteConnection = function(edge){
  $.ajax({ 
    type: 'POST',
    url: '/delete_edge/',
    async: false,
    data: {
      "edge_id":edge.item_id
    },
    success: function () {
      navigo.connectedNodes(navigo.connectedNodes().filter(function(a){
        return a.item_id != edge.item_id
      }));
    },
    error: function(){}
  })
}

navigo.loadCorpusData = function(data) {
  navigo.currentCorpus(data);
      var url = "http://192.168.0.61:80" + data.image;
      // debugger;
      $('#cy').css('background-image', "url('" + url + "')")
      $.ajax({
          type: 'POST',
          url: '/get_areas/',
          data: {
              "floor":data.floor,
              "corpus":data.id,
              "img_url":url
          },
          success: function (responce) {
              $('#cy').css({'width':responce.img_width,'height': responce.img_height})
              cy.remove(cy.elements())
              console.log(responce.areas)
              navigo.nodes(responce.areas)
              var eles = cy.add(responce.areas);
              console.log(responce.edges)
              navigo.edges(responce.edges)
              var eles = cy.add(responce.edges);
              cy.elements('edge[blocked>0]').addClass('blocked');
              cy.elements('node[area_type="point"]').classes('area');
          },
          error: function(xhr, ajaxOptions, thrownError){
              console.log('getPoints.post');
          }
      })
};


navigo.loadCorpusConnectNodes = function(data) {
  navigo.selectedCorpusModal(data);
  $.ajax({
      type: 'POST',
      url: '/get_connect_nodes/',
      data: {
          "corpus":data.id,
      },
      success: function (responce) {
        navigo.connectNodesList(responce);
      },
      error: function(){}
  })
};

navigo.connectNodes = function(){
  $.ajax({
    type: 'POST',
    url: '/connect_nodes/',
    data: {
      "area_left_id": navigo.editingNode().data.item_id,
      // "image_left": navigo.image_left(),
      //  "text_left": navigo.text_left(),
      "area_right_id": navigo.selectedConnectingNode().item_id,
      // "image_right": navigo.image_right(),
      //  "text_right": navigo.text_right(),
      //  "distance":navigo.distance(),
       "corpus": navigo.currentCorpus().id,
    },
    success: function (responce) {
      navigo.selectedConnectingNode(null);
      navigo.selectedCorpusModal(null);
    },
    error: function(xhr, ajaxOptions, thrownError){
      console.log('addArea.post');
    }
  })
}


 navigo.addArea = function(){
    $.ajax({
      type: 'POST',
      url: '/add_edit_area/',
      data: {
        "auditories": navigo.title(),
        "edge": null,
        "corpus": navigo.currentCorpus().id,
        "area_type": navigo.areaType().key,
        "position_top": navigo.y(),
        "position_left": navigo.x(),
        "floor":navigo.currentCorpus().floor,
        "isNewNode": 1
      },
      success: function (newNode) {
        var node = {
          group: "nodes",
          data: { id: newNode.title, item_id: newNode.id, area_type: newNode.area_type },
          position: { x: parseInt(newNode.position_left), y: parseInt(newNode.position_top) }
        }
        navigo.nodes.push(node)
        cy.add(node);
        if(navigo.areaType() == "point"){
          cy.elements('node[item_id='+newNode.id+']')[0].addClass('area');
        }
      },
      error: function(xhr, ajaxOptions, thrownError){
          console.log('addArea.post');
      }
  })
 }


 navigo.corpusList_filtered = ko.computed(function(){
   return navigo.corpusList().filter(function(a){
     return a.name.indexOf(navigo.searchCorpus()) != -1 ||
            a.address.indexOf(navigo.searchCorpus()) != -1 ||
            (a.floor.toString()+' этаж').indexOf(navigo.searchCorpus()) != -1
   })
 })

 navigo.corpusList_filtered2 = ko.computed(function(){
  return navigo.corpusList().filter(function(a){
    return a != navigo.currentCorpus() &&
          (a.name.indexOf(navigo.searchCorpus2()) != -1 ||
           a.address.indexOf(navigo.searchCorpus2()) != -1 ||
           (a.floor.toString()+' этаж').indexOf(navigo.searchCorpus2()) != -1)
  })
})

 navigo.addCorpus = function(){
  formdata = new FormData();     
  formdata.append("image", $("#image_to_upload")[0].files[0]);
  formdata.append("name", navigo.corpus_name());
  formdata.append("floor", navigo.corpus_floor());
  formdata.append("address", navigo.corpus_address_selected());
  formdata.append("mode", "create");
      $.ajax({ 
        type: 'POST',
        url: '/add_update_courpus/',
        data: formdata,
        contentType: false,
        processData: false,
        success: function (responce) {
          $('#add_corpus form')[0].reset()
          navigo.corpus_name('')
          navigo.corpus_floor('')
          navigo.corpus_address_selected('')
          navigo.getCorpuses();
        },
        error: function(xhr, ajaxOptions, thrownError){
            console.log('deleteArea.post');
        }
      })
}


navigo.updateCorpus = function(){
  formdata = new FormData();     
  formdata.append("image", $("#image_to_upload")[0].files[0]);
  formdata.append("name", navigo.corpus_name());
  formdata.append("floor", navigo.corpus_floor());
  formdata.append("address", navigo.corpus_address_selected());
  formdata.append("mode", "update");
  $.ajax({ 
    type: 'POST',
    url: '/add_update_courpus/',
    data: formdata,
    contentType: false,
    processData: false,
    success: function (responce) {
      $('#add_corpus form')[0].reset()
      navigo.corpus_name('')
      navigo.corpus_floor('')
      navigo.corpus_address_selected('')
      navigo.getCorpuses();
    },
    error: function(xhr, ajaxOptions, thrownError){
        console.log('deleteArea.post');
    }
  })
}




navigo.updateArea = function(){
  $.ajax({
    type: 'POST',
    url: '/add_edit_area/',
    data: {
      "area_id": navigo.editingNode().data.item_id,
      "auditories": navigo.title(),
      "edge": null,
      "corpus": navigo.corpus().val,
      "area_type": navigo.areaType().key,
      "position_top": navigo.y(),
      "position_left": navigo.x(),
      "floor":navigo.floor().val,
      "isNewNode": 0
    },
    success: function (newNode) {
      var node = {
        group: "nodes",
        data: { id: newNode.title, item_id: newNode.id, area_type: newNode.area_type },
        position: { x: parseInt(newNode.position_left), y: parseInt(newNode.position_top) }
      }
      navigo.nodes().filter(function(a){return a.data.item_id == navigo.editingNode().data.item_id})[0].data = node.data
      cy.elements('node[item_id='+navigo.editingNode().data.item_id+']')[0].data(node.data)
      // cy.nodes("#"+navigo.editingNode().data.id).data(node.data)
    },
    error: function(xhr, ajaxOptions, thrownError){
        console.log('updateArea.post');
    }
})
}


navigo.deleteNode = function(){

  cy.elements("edge[source = '"+navigo.editingNode().data.id+"']").forEach(function(a){
    navigo.editingEdge({'data':a.data()})
    navigo.deleteEdge()
  })
  cy.elements("edge[target = '"+navigo.editingNode().data.id+"']").forEach(function(a){
    navigo.editingEdge({'data':a.data()})
    navigo.deleteEdge()
  })

  $.ajax({
    type: 'POST',
    url: '/delete_area/',
    async: false,
    data: {
      "node_id":navigo.editingNode().data.item_id
    },
    success: function () {
      navigo.nodes.remove(navigo.editingNode())
      cy.remove( cy.$("#"+navigo.editingNode().data.id) );
    },
    error: function(xhr, ajaxOptions, thrownError){
        console.log('deleteArea.post');
    }
})
}


navigo.addEdge = function(){
  $.ajax({
    type: 'POST',
    url: '/add_edit_edge/',
    data: {
      "area_left_id": navigo.area_left().data.item_id,
     // "image_left": navigo.image_left(),
      "text_left": navigo.text_left(),
      "area_right_id": navigo.area_right().data.item_id,
     // "image_right": navigo.image_right(),
      "text_right": navigo.text_right(),
      "distance":navigo.distance(),
      "corpus": navigo.currentCorpus().id,
      "isNewEdge": 1
    },
    success: function (newEdge) {
      // navigo.area_left(null);
      // navigo.area_right(null);
      var edge = {
        group: "edges",
        data: newEdge
      }
      navigo.edges.push(edge);
      cy.add(edge);
      navigo.dropSelection();
    },
    error: function(xhr, ajaxOptions, thrownError){
        console.log('addArea.post');
    }
})
}

navigo.updateEdge = function(){
  $.ajax({
    type: 'POST',
    url: '/add_edit_edge/',
    data: {
      "edge_id": navigo.editingEdge().data.item_id,
      "area_left_id": navigo.area_left().data.item_id,
      //"image_left": navigo.image_left(),
      "text_left": navigo.text_left(),
      "area_right_id": navigo.area_right().data.item_id,
      //"image_right": navigo.image_right(),
      "text_right": navigo.text_right(),
      "distance":navigo.distance(),
      "corpus": navigo.corpus().val,
      "isNewEdge": 0
    },
    success: function (newEdge) {
      navigo.edges().filter(function(a){return a.data.item_id == navigo.editingEdge().data.item_id})[0].data = newEdge
      cy.elements('edge[item_id='+navigo.editingEdge().data.item_id+']')[0].data(newEdge)
      // cy.nodes("#"+navigo.editingEdge().data.id).data(edge.data)
    },
    error: function(xhr, ajaxOptions, thrownError){
        console.log('updateArea.post');
    }
  })
}




navigo.deleteEdge = function(){
  $.ajax({ 
    type: 'POST',
    url: '/delete_edge/',
    async: false,
    data: {
      "edge_id":navigo.editingEdge().data.item_id
    },
    success: function () {
      // debugger;
      navigo.edges(navigo.edges().filter(function(a){ return a.data.id != navigo.editingEdge().data.id}));
      cy.remove( cy.$("#"+navigo.editingEdge().data.id) );
      navigo.area_left(null);
      navigo.area_right(null);
      navigo.editingEdge(null);
    },
    error: function(xhr, ajaxOptions, thrownError){
        console.log('deleteArea.post');
    }
  })
}

navigo.createEdgeModal = function(){ 
  navigo.flagForEditEdge(false); 
  // navigo.title('create edge'); 
  navigo.area_left('')
  navigo.area_right('')
  navigo.text_left('')
  navigo.text_right('')
}

navigo.zooming = function(zoom){
  //height = $('#cy, #cy>div, #cy>div>canvas').height()
  //width = $('#cy, #cy>div, #cy>div>canvas').width()
  navigo.zoom(zoom);
  cy.zoom(zoom);
  $('#cy, #cy>div, #cy>div>canvas').css({'height':zoom*600 + 'px', 'width': zoom*800 + 'px'})
}

var cy = window.cy = cytoscape({
    container: document.getElementById('cy'),
  
    boxSelectionEnabled: false,
    autounselectify: true,
    zoomingEnabled: false,
    userZoomingEnabled: false,
    userPanningEnabled: false,
  
    layout: {
      name: 'dagre'
    },
  
    style: [
      {
        selector: 'node',
        style: {
          'width': 10,
          'height': 10,
          'content': '',
          'text-opacity': 1,
          'text-valign': 'center',
          'text-halign': 'right',
          'background-color': '#11479e'
        }
      },
      {
        selector: 'edge',
        style: {
          'curve-style': 'bezier',
          'width': 2,
          //'target-arrow-shape': 'circle',
          'line-color': '#9dbaea',
          'target-arrow-color': '#9dbaea'
        }
      },
      {
        selector: '.area',
        style: {
          'content': 'data(id)'
        }
      },
      {
        selector: '.selectedNode',
        style: {
          'width': 15,
          'height': 15,
          'background-color': '#86bff4'
        }
      },
      {
        selector: '.onHoverTargetEdge',
        style: {
          'width': 20,
          'height': 20,
          'background-color': '#86bff4'
        }
      },
      {
        selector: '.onHoverTargetNode',
        style: {
          'width': 5,
        }
      },
      {
        selector: '.blocked',
        style: {
          'line-color': '#ff0000',
        }
      }
      
    ],
  
    elements: {
      nodes: [],
      edges: []
    },
  });
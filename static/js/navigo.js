navigo = {};

navigo.from = ko.observable('');
navigo.to = ko.observable('');
navigo.way = ko.observableArray([]);
navigo.floor = ko.observable('');
navigo.corpus = ko.observable('');
navigo.x = ko.observable(0);
navigo.y = ko.observable(0);
navigo.zoom = ko.observable(1);
navigo.editingNode = ko.observable({})
navigo.editingEdge = ko.observable({})
navigo.nodes = ko.observableArray([])
navigo.edges = ko.observableArray([])
navigo.area_left = ko.observable({});
navigo.image_left = ko.observable('');
navigo.text_left = ko.observable('');
navigo.area_right = ko.observable({});
navigo.image_right = ko.observable('');
navigo.text_right = ko.observable('');
navigo.distance = ko.observable(0);
navigo.title = ko.observable("");
navigo.areaType = ko.observable("");
navigo.search = ko.observable('');
navigo.searchValue = ko.observable('');
navigo.flagForEditNode = ko.observable(false)
navigo.flagForEditEdge = ko.observable(false)
navigo.pointsList = ko.observableArray([]);
navigo.searchCorpus = ko.observable('');
navigo.searchCorpus2 = ko.observable('');
navigo.currentCorpus = ko.observable({});
navigo.creationEdgeMode = ko.observable(false);
navigo.deletionEdgeMode = ko.observable(false);
navigo.blockEdgeMode = ko.observable(false);
navigo.defaultMode = ko.observable(true);
navigo.isMenuHidden = ko.observable(false);
navigo.isNodeDraging = ko.observable(false);
navigo.connectNodesList = ko.observableArray([]);
navigo.selectedCorpusModal = ko.observable(null);
navigo.selectedConnectingNode = ko.observable(null);
navigo.connectedNodes = ko.observableArray([]);
navigo.modalTitle = ko.observable('');
// navigo.tempSelectedNode = ko.observable(null);
navigo.areaTypes = ko.observableArray([
    {"key":"point", "value":'Ключевая точка'},
    {"key":"connect", "value":'Соединительная точка'},
    {"key":"corner", "value":'Коридор с поворотом'}, 
    {"key":"straight", "value":'Прямой коридор'}
]);

navigo.activeStep = ko.observable(0)

navigo.editMode_corpus = ko.observable(false);
navigo.corpusList = ko.observableArray([]);
navigo.corpus_name = ko.observable('');
navigo.corpus_floor = ko.observable('');
navigo.corpus_address_selected = ko.observable('');
navigo.corpus_address = ko.observableArray(['ул.Мира 19','просп.Ленина 51','ул.Куйбышева 48','ул.Софьи Ковалевской 5']);

navigo.validFormCorpus = ko.computed(function(){
    if(navigo.corpus_address_selected() != undefined && 
        navigo.corpus_floor() > -1 && 
        navigo.corpus_name().trim().length > 0 &&
        $('#image_to_upload')[0].files.length > 0){
            return true;
        }

    return false;
})



navigo.selectPoint = function(value){
    navigo.searchValue(value);

    if(value == 'from' && navigo.from().length > 0){
        navigo.search(navigo.from())
    } else if(value == 'to' && navigo.to().length > 0){
        navigo.search(navigo.to())
    }

    // $("#search_point").width(window.screen.width);
    // $("#search_point").height(window.screen.height);
    // $("#search_point").css("min-width", window.screen.width);
    // $("#search_point").css("min-height", window.screen.height);
    $('#search_point').modal('open');
    $('#n_input').focus();
}

navigo.matchPointsList = ko.computed(function(){

    var _length = 0;
    if (navigo.search().length == 0)
        return [];

    setTimeout(function(){
        var myScroll = new IScroll('#iscroll1',{
            mouseWheel: true
        });
        $('.n-search-item > div').off('tap');
        $('.n-search-item > div').on('tap', function(e, data) { 
            console.log(data[0].target.innerText)

            if(navigo.searchValue() == 'from')
                navigo.from(data[0].target.innerText)
            else
                navigo.to(data[0].target.innerText)

            navigo.search("")
            $('#search_point').modal('close')
        });
    },100)

    return navigo.pointsList().filter(function(a, b){
        if (a.toLowerCase().indexOf(navigo.search().toLowerCase()) != -1 & _length < 30){
            _length++;
            return true;
        } else {
            return false;
        }
    })
})

navigo.auditories = ko.observableArray([])
// вызвать функцию в консоли для обновления списка аудиторий
navigo.getAuditoriesForForm = function(){
    $.ajax({
        type: 'POST',
        url: '/get_auditories_for_form/',
        success: function (responce) {
            navigo.auditories(responce)
            navigo.pointsList(responce)
        },
        error: function(xhr, ajaxOptions, thrownError){
            console.log('getAuditoriesForForm.post');
        }
    })

}



navigo.start = function() {

    $.ajax({
        type: 'POST',
        url: '/find_way/',
        data: {
            "from":navigo.from(),
            "to":navigo.to(),
            "_width": window.screen.availWidth-20,
            "_height": window.screen.availHeight-100
        },
        success: function (responce) {

            $('.n-mobile-mobal .slider').height(window.screen.height - 100);
            $('#find_way').modal('open');
            // debugger;
            responce.forEach(function(a, b) {
                if(!a.yandexAPI){
                    // debugger;
                    a.nodes = a.nodes.filter(function(c,d){
                        if(a.nodes[d].data.area_type == "point" || (a.nodes[d-1] && a.nodes[d-1].data.area_type == "point")){
                            return true;
                        }
                        if(a.nodes[d].data.area_type == "straight" && (a.nodes[d+1].data.area_type == "straight" || a.nodes[d+1].data.area_type == "corner")){
                            return false;
                        } 
                        return true;
                    })
                    // for(var i=0; i< a.nodes.length-1; i++){
                    //     if(a.nodes[i].data.area_type == "stright" && a.nodes[i+1].data.area_type == "stright"){

                    //     }
                    // }
                }
            });
            navigo.way(responce)
            navigo.way().forEach(function(a, b) {
                if(a.yandexAPI){
                    initMap(a.points, b);
                } else {
                    edges = []
                    for(var i=0; i< a.nodes.length-1; i++){
                        edges.push({ group: "edges", data: { id: "edge"+i, source: a.nodes[i].data.id, target: a.nodes[i+1].data.id } })
                    }
                    navigo.setCytoScape('cy'+b, edges, a.nodes, a.img_width, a.img_height, a.zooming)
                }
            });
            
            navigo.activeStep(0)
            
            setTimeout(function(){
                $('.one-time').slick({
                    dots: true,
                    mobileFirst: true,
                    infinite: false,
                    arrows: false,
                    speed: 300,
                    slidesToShow: 1,
                    adaptiveHeight: true
                });
            },50)

        },
        error: function(xhr, ajaxOptions, thrownError){
            console.log('find_way.post');
        }
    })
}


navigo.getCorpuses = function(){
    $.ajax({ 
      type: 'GET',
      url: '/get_corpuses/',
      success: function (responce) {
        navigo.corpusList(responce)
      },
      error: function(xhr, ajaxOptions, thrownError){
          console.log('deleteArea.post');
      }
    })
   }
// navigo.asd = ko.observableArray([]);

navigo.setCytoScape = function(id, edges, nodes, img_width, img_height, zooming){
     cy = cytoscape({
        container: document.getElementById(id),

        boxSelectionEnabled: false,
        autounselectify: false,
        panningEnabled: false,
        userPanningEnabled: false,
        zoomingEnabled: true,
        userPanningEnabled: false,
        autolock: true,
      
        layout: {
          name: 'dagre'
        },
      
        style: [
          {
            selector: 'node',
            style: {
              'width': 10*zooming,
              'height': 10*zooming,
              'content': 'data(id)',
              'text-opacity': 0,
            //   'text-valign': 'center',
            //   'text-halign': 'right',
              'background-color': '#000'
            }
          },
          {
            selector: '.first',
            style: {
              'width': 20*zooming,
              'height': 20*zooming,
              'content': 'Вы здесь',
              'font-size': 12,
            //   'text-valign': 'top',
            //   'text-halign': 'center',
              'text-opacity': 1,
            }
          },
      
          {
            selector: 'edge',
            style: {
              'curve-style': 'bezier',
              'width': 1.5,
              'target-arrow-shape': 'triangle',
              'line-color': '#000',
              'target-arrow-color': '#000',
              'arrow-scale': 0.7
            }
          }
        ],
      
        elements: {
          nodes: [],
          edges: []
        },
      });
    cy.add(nodes)
    cy.add(edges)

    cy.elements('node[item_id='+nodes[0].data.item_id+']')[0].addClass('first');
    // navigo.asd.push(cy);
    $('#'+id).css({"width": img_width, "height": img_height});
}


function initMap (points, index) {
    
    var multiRoute = new ymaps.multiRouter.MultiRoute({
        referencePoints: points,
        params: {
            routingMode: 'masstransit'
        }
    }, {
        // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
        boundsAutoApply: true
    });

    // Создаем карту с добавленной на нее кнопкой.
    var myMap = new ymaps.Map('map'+index, {
        center: [56.833333, 60.583333],
        zoom: 12,
        controls: []
    });

    // Добавляем мультимаршрут на карту.
    myMap.geoObjects.add(multiRoute);
    myMap.behaviors.disable('drag');
}


$( document ).ready(function() {
        // for csrf
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });


    $('.modal').modal({
        // ready: function(modal, trigger) {
        //     if(modal.attr('id') == "find_way" || modal.attr('id') == "search_point"){
        //         modal.width(window.screen.width);
        //         modal.height(window.screen.height);
        //         modal.css("min-width", window.screen.width);
        //         modal.css("min-height", window.screen.height);
        //     }
        // },
        complete: function(e){
            if(e.attr('id') == "find_way"){
                navigo.way([]);
                $('.one-time').slick('unslick');
                $('.carousel-item').remove();
            }
        }
    });

    $("#find_way, #search_point").width(window.screen.width);
    $("#find_way, #search_point").height(window.screen.height);
    $("#find_way, #search_point").css("min-width", window.screen.width);
    $("#find_way, #search_point").css("min-height", window.screen.height);
    
    // ymaps.ready(initMap);
    

    console.log( "ready!" );
    ko.applyBindings(navigo);
    navigo.getAuditoriesForForm();
    navigo.getCorpuses();

});

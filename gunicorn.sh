#!/bin/bash
set -e
SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOGFILE=$SCRIPTPATH/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3
# user/group to run as
USER=root
GROUP=root
PORT=8008
cd /home/sckottjackson/Working/Navigo/project/src
source ../../venv/bin/activate
test -d $LOGDIR || mkdir -p $LOGDIR
exec ../../venv/bin/gunicorn wsgi:application -w $NUM_WORKERS \
  --user=$USER --group=$GROUP --log-level=debug \
  --log-file=$LOGFILE 2>>$LOGFILE \
  --bind 127.0.0.1:$PORT

